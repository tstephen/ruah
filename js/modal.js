var closeBtn = document.getElementById('closeBtn');
var backdrop = document.querySelector('.backdrop');
var modalContent = document.querySelector('.modal-content');
var moreInfo = document.querySelector('.arrow');
var imageComments = document.querySelector('.image-comments');
var like = document.querySelector('.like');

var closeModal = function() {
	backdrop.style.display = 'none';
	modalContent.style.display = 'none';
}

closeBtn.addEventListener('click', closeModal);

backdrop.addEventListener('click', closeModal);

moreInfo.addEventListener('click', function() {
	imageComments.style.display = 'block';
	moreInfo.style.display = 'none';
});

like.addEventListener('click', function() {
	var likeImageForm = document.getElementById('likeImage');
	likeImageForm.submit();
});