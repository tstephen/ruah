// Global vars
let width = 750,
	height = 0,
	filter = 'none',
	streaming = false;

// DOM Elements
const video = document.getElementById('video');
const canvas = document.getElementById('canvas');
//const photos = document.getElementById('photos');
//const photoButton = document.getElementById('photo-button');
//const clearButton = document.getElementById('clear-button');
//const photoFilter = document.getElementById('photo-filter');
const images = document.getElementsByClassName('user-images');
const mainPage = document.getElementById('main-page');
const stickers = document.getElementsByClassName('sticker-image');
const webcamButton = document.getElementById('webcam-button');
const uploadButton = document.getElementById('upload-button');
const submitButton = document.querySelector('#submit-button');

// For setInterval on webcam
var timer;

if (canvas.style.display == '' || canvas.style.display == 'none') {
	submitButton.style.display = 'none';
}

var videoHeight;

for (var i = 0; i < images.length; i++) {
	images[i].addEventListener('click', selectUserImage);
}

width = mainPage.offsetWidth - 4;
var context = canvas.getContext('2d');

function selectUserImage(event) {
	canvas.style.display = 'block';
	video.style.display = 'none';

	// Reset all previous user images opacity's
	for (var i = 0; i < images.length; i++) {
		images[i].style.opacity = 1;
	}
	// Set current selected image opacity
	event.target.style.opacity = 0.5;
	
	var imageObj = new Image();

	// Set image object to user image
	imageObj.src = event.target.src;
	height = imageObj.naturalHeight / (imageObj.naturalWidth / width);
	context.clearRect(0, 0, canvas.width, canvas.height);
	width = mainPage.offsetWidth - 4;
	canvas.setAttribute("width", width);
	canvas.setAttribute("height", height);
	context.drawImage(imageObj, 0, 0, width, height);
	submitButton.style.display = 'block';
}

for (var i = 0; i < stickers.length; i++) {
	stickers[i].addEventListener('click', applySticker);
}

function applySticker(event) {
	var imageObj = new Image();
	imageObj.src = event.target.src;
	height = imageObj.naturalHeight / (imageObj.naturalWidth / width);
	context.drawImage(imageObj, 0, 0, canvas.width, canvas.height);
	submitButton.style.display = 'block';
}


webcamButton.addEventListener('click', function() {
	// Get media stream
	if (!streaming) {
		navigator.mediaDevices.getUserMedia({video: true, audio: false})
		.then(function(stream) {
			// Link to the video source
			video.srcObject = stream;
			video.play();
		})
		.catch(function(err) {
			console.log(`Error: ${err}`);
		});
	}

	context.clearRect(0, 0, canvas.width, canvas.height);
	video.style.display = 'block';

	// Play when ready
	width = mainPage.offsetWidth - 4;
	videoHeight = video.videoHeight / (video.videoWidth / width);
	canvas.setAttribute("width", width);
	canvas.setAttribute("height", videoHeight);
	video.addEventListener('canplay', function() {
		width = mainPage.offsetWidth - 4;
		videoHeight = video.videoHeight / (video.videoWidth / width);
		video.setAttribute("width", width);
		video.setAttribute("height", videoHeight);
		canvas.setAttribute("width", width);
		canvas.setAttribute("height", videoHeight);
		canvas.style.display = 'inline';
	}, false);
	streaming = true;
	submitButton.style.display = 'block';
});

uploadButton.addEventListener('change', handleImage, false);

function handleImage(event) {
	canvas.style.display = 'block';
	video.style.display = 'none';
	var reader = new FileReader();
    reader.onload = function(event) {
		var img = new Image();
		img.src = event.target.result;
        img.onload = function() {
			width = mainPage.offsetWidth - 4;
			height = img.naturalHeight / (img.naturalWidth / width);
			context.clearRect(0, 0, canvas.width, canvas.height);
			canvas.setAttribute("width", width);
			canvas.setAttribute("height", height);
            context.drawImage(img,0,0, width, height);
        }
    }
	reader.readAsDataURL(event.target.files[0]);
	submitButton.style.display = 'block';
}

function saveImage() {
	document.getElementById('inp_img').value = canvas.toDataURL();
	if (!(video.style.display === 'none')) {
		var newCanvas = document.createElement('canvas');
		newCanvas.id = "CursorLayer";
		newCanvas.width = canvas.width;
		newCanvas.height = canvas.height;

		var newContext = newCanvas.getContext('2d');
		newContext.drawImage(video, 0, 0, canvas.width, canvas.height);
		document.getElementById('video_img').value = newCanvas.toDataURL();
		document.getElementById('img-width').value = canvas.width;
		document.getElementById('img-height').value = canvas.height;
	}
}

submitButton.addEventListener('click', saveImage, true);
