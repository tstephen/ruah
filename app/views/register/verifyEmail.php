<?php $this->start('head'); ?>
<?php $this->setSiteTitle('Camagru | Verify Email'); ?>
<?php $this->end(); ?>
<?php $this->start('body'); ?>
<h1>Please confirm your email address</h1><hr/>
<p>The email address associated with your account is <span class="emailAddress"><?=$this->emailToBeConfirmed?></span>,
if this is correct and you have not received the verification email as of yet, you may resend the email by pressing the button below.<p/><hr/>
<div style="text-align: center;">
	<form action="<?=PROOT?>register/resendConfirmEmail" method="POST">
		<button class="btn btn-lg btn-success" name="resendConfirmEmail" value="<?=$this->username?>">Resend Verification Email</button>
	</form>
</div>
<?php $this->end(); ?>