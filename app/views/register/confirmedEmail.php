<?php $this->start('head'); ?>
<?php $this->setSiteTitle('Camagru | Verified Email'); ?>
<?php $this->end(); ?>
<?php $this->start('body') ?>
	<?php if ($this->validatedEmail == 1) : ?>
		<h1>Thank you for validating your email address, you may proceed to login.</h1><hr/>
	<?php elseif ($this->validatedEmail == 0) : ?>
		<h1>Something went wrong, to resend the verification email click the button below.</h1><hr/>
		<div style="text-align: center;">
			<form action="<?=PROOT?>register/resendConfirmEmail" method="POST">
				<button class="btn btn-lg btn-success" name="resendConfirmEmail" value="<?=$this->username?>">Resend Verification Email</button>
			</form>
		</div>
	<?php endif;  ?>
<?php $this->end(); ?>