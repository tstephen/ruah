<?php $this->start('head'); ?>
<?php $this->setSiteTitle('Camagru | Forgot Password'); ?>
<?php $this->end(); ?>
<?php $this->start('body'); ?>
<!-- form to create new password -->
<?php if ($this->resetPassword == 1) : ?>
	<div class="col-md-6 col-md-offset-3 well">
		<form class="form" action="<?=''//PROOT . "register" . DS . "forgotPassword" . DS . $this->username . DS . $this->resetPasswordToken?>" method="POST">
			<div class="bg-danger"><?=$this->displayErrors?></div>
			<div class="form-group">
				<label for="newPassword">New Password</label>
				<input type="password" name="newPassword" class="form-control" id="newPassword">
			</div>
			<div class="form-group">
				<label for="confirmNewPassword">Confirm Password</label>
				<input type="password" name="confirmNewPassword" class="form-control" id="confirmNewPassword">
			</div>
			<div class="form-group text-center">
				<input class="btn btn-larger btn-success" type="submit" value="Confirm New Password">
			</div>
		</form>
	</div>
<!-- form to submit email address --> 
<?php elseif ($this->resetPassword == 0) : ?>
	<div class="col-md-6 col-md-offset-3 well">
		<form class="form" action="" method="POST">
			<div class="form-group text-center lead">
				<label for="resetPasswordUsername" style="margin-bottom: 0">Enter Your Username Below</label><hr/>
				<input type="text" name="resetPasswordUsername" class="form-control" id="resetPasswordUsername">
			</div>
			<div class="form-group text-center" style="margin-top: 3%;">
				<input class="btn btn-larger btn-success" type="submit" value="Reset Your Password">
			</div>
		</form>
	</div>
<?php endif; ?>
<?php $this->end(); ?>