<?php $this->start('head'); ?>
<?php $this->setSiteTitle("Camagru | Profile"); ?>
<?php $this->end(); ?>

<?php $this->start('body'); ?>
<div class="col-md-6 col-md-offset-3 well">
	<h1 class="text-center">Update Profile</h1><hr>
	<form class="form" action="<?=PROOT?>register/profile" method="post">
		<?php if (isset($this->displayErrors)) : ?>
			<div class="bg-danger"><?=$this->displayErrors?></div>
		<?php endif; ?>
		<p>Modify values that you want to update! Password is required.</p>
		<div class="form-group">
			<label for="username">Username</label>
			<input type="text" name="username" id="username" class="form-control" value="<?=$this->username;?>">
		</div>
		<div class="form-group">
			<label for="email">Email</label>
			<input type="email" name="email" class="form-control" id="email" value="<?=$this->email;?>">
		</div>
		<div class="form-group">
			<label for="password">Password</label>
			<input type="password" name="password" id="password" class="form-control">
		</div>
		<div class="form-group">
			<label for="confirm">Confirm Password</label>
			<input type="password" name="confirm" class="form-control" id="confirm" value="">
		</div>
		<div class="form-group">
			<label for="notifications">Receive Notifications<input type="checkbox" name="notifications" id="notifications" <?=$this->notifications;?>></label>
		</div>
		<div class="form-group">
			<input type="submit" value="Update" class="btn btn-large btn-primary">
		</div>
	</form>
</div>
<?php $this->end(); ?>