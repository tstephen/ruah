<?php $this->start('head'); ?>
	<link rel="stylesheet" href="<?=PROOT?>css/modal.css" media="screen" title="no title" charset="utf-8">
    <style>
        div.gallery {
            margin: 0;
            padding: 0;
            border: 1px solid #ccc;
            display: inline-grid;
            width: 48%;
        }

        div.gallery:hover {
            border: 1px solid #222;
            transition: 0.2s;
            transform: scale(1.05);
        }

        div.gallery img {
            width: 100%;
            height: auto;
        }

        div.gallery::after {
            content: "";
            display: block;
            clear: both;
        }

        div.text-center::before {
            display: block;
            content: "";
            clear: both;
        }
    </style>
<?php $this->end(); ?>

<?php $this->start('body'); ?>

    <!-- Modal -->
    <?php if (isset($this->viewImageType) && isset($this->viewImage) && isset($this->viewImageName)) : ?>
        <div class="backdrop"></div>
        <div class="modal-content">
            <div class="modal-header">
                <form id="likeImage" action="<?=PROOT?>gallery/imageInfo/<?=$this->currentPageNumber; ?>/<?=$this->viewImageId?>" method="POST">
                    <input type="submit" name="like" value="<?=$this->numLikes?>" class="like btn btn-danger">
                </form>	
                <button id="closeBtn" type="button" class="close">&times;</button>
            </div>
            <div class="modal-body">
                <img class="img-responsive" src="data:<?=$this->viewImageType ?>;base64, <?=$this->viewImage?>" alt="<?=$this->viewImageName?>">
                <ul class="image-comments">
                    <?php foreach ($this->comments as $comment) : ?>
                        <li><?=$comment->user_id?> : </li>
                        <blockquote><?=$comment->comment?></blockquote>
                    <?php endforeach; ?>
                    <?php if (currentUser()) : ?>
                        <form id="commentImage" action="<?=PROOT?>gallery/imageInfo/<?=$this->currentPageNumber; ?>/<?=$this->viewImageId?>" method="POST">
                            <textarea name="commentText" rows="5"></textarea>
                            <input type="submit" value="Post Comment">
                        </form>
                    <?php endif; ?>
                </ul>
            </div>

            <div class="modal-footer">
                <div class="arrow bounce"></div>
            </div>
        </div>
        <script src="<?=PROOT?>js/modal.js"></script>
    <?php endif; ?>

    <!-- Gallery -->
    <div class="container">
    <?php foreach ($this->images as $image) : ?>
        <div class="gallery">
            <a href="<?=PROOT?>gallery/imageInfo/<?=$this->currentPageNumber; ?>/<?=$image->id?>">
                <img class="image-responsive" src="data:<?=$image->type ?>;base64, <?=$image->image?>" width="600" height="400" alt="<?=$image->name?>">
            </a>
        </div>	
    <?php endforeach; ?>
    </div>

    <!-- Pagination -->
    <div class="text-center">
            <ul class="pagination pagination-lg">
                <li><a href="<?=PROOT?>gallery/gallery/1"><<</a></li>
                <?=$this->pages;?>
                <li><a href="<?=PROOT?>gallery/gallery/<?=$this->lastPageNumber?>">>></a></li>
            </ul>
    </div>
<?php $this->end(); ?>