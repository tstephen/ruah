<?php $this->start('head'); ?>
<?php $this->setSiteTitle('Camagru | Upload Image'); ?>
<link rel="stylesheet" href="<?=PROOT?>css/upload.css" media="screen" title="no title" charset="utf-8">
<?php $this->end(); ?>
<?php $this->start('body'); ?>
	<!-- BOOTSTRAP GRID -->
	<div class="container">
		<div class="row">
			<div id="left-sidebar" class="col-md-3">
				<p>Stickers</p>
				<img class="sticker-image img-responsive" width="600" height="400" src="<?=PROOT?>img/stickers/golden-frame.png">
				<img class="sticker-image img-responsive" width="600" height="400" src="<?=PROOT?>img/stickers/colorful-frame.png">
				<img class="sticker-image img-responsive" width="600" height="400" src="<?=PROOT?>img/stickers/navyblue-frame.png">
				<img class="sticker-image img-responsive" width="600" height="400" src="<?=PROOT?>img/stickers/christmas-frame.png">
			</div>
			<div id="main-page" class="col-md-6">
				<p>Edit Photo's</p>
				<button id="webcam-button">Use Webcam</button>
				<input type="file" id="upload-button" name="imageLoader"/>
				<canvas id="canvas"></canvas>
				<video id="video">Stream not available ...</video>
				<form class="form" action="" method="POST" enctype="multipart/form-data">					
					<input id="inp_img" name="canvas-img" type="hidden" value="">
					<input id="video_img" name="video-img" type="hidden" value="">
					<input id="img-width" name="img-width" type="hidden" value="">
					<input id="img-height" name="img-height" type="hidden" value="">
					<input type="submit" name="savePicture" class="disabled" id="submit-button" value="Submit Image">
				</form>
			</div>
			<div id="right-sidebar" class="col-md-3">
				<p><?=currentUser()->fname;?>'s Images</p>
				<?=$this->userImages?>
			</div>
		</div>
	</div>
	<script src="<?=PROOT?>js/upload.js"></script>
<?php $this->end(); ?>