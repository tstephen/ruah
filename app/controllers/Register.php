<?php
	class Register extends Controller {
		public function __construct($controller, $action) {
			parent::__construct($controller, $action);
			$this->load_model('Users');
			$this->view->setLayout('default');
		}

		public function loginAction() {
			$validation = new Validate();

			if ($_POST) {
				// form validation
				$validation->check($_POST, [
					'username' => [
						'display' => "Username",
						'required' => True,
					],
					'password' => [
						'display' => 'Password',
						'required' => True,
						'min' => 6
					]
				]);

				if ($validation->passed()) {			
					$user = $this->UsersModel->findByUsername($_POST['username']);				
					if ($user->username == $_POST['username'] && password_verify(Input::get('password'), $user->password)) {
						if ($user->isEmailConfirmed == 1) {
							$remember = (isset($_POST['remember_me']) && Input::get('remember_me')) ? True : False;
							$user->login($remember);
							Router::redirect('');
						} else {
							$this->view->emailToBeConfirmed = $user->email;
							$this->view->username =$user->username;
							$this->view->render('register/verifyEmail');
							return ;
						}
					} else {
						$validation->addError("There is an error with your username or password.");
					}
				}
			}
			$this->view->displayErrors = $validation->displayErrors();
			$this->view->render('register/login');
		}

		public function logoutAction() {
			if (currentUser()) {
				currentUser()->logout();
			}
			Router::redirect('');
		}

		public function registerAction() {
			$validation = new Validate();
			$posted_values = [
				'fname' => '',
				'lname' => '',
				'username' => '',
				'email' => '',
				'password' => '',
				'confirm' => ''
			];

			if ($_POST) {
				$posted_values = posted_values($_POST);
				$validation->check($posted_values, [
					'fname' => [
						'display' => 'First Name',
						'required' => True,
						'special_chars' => 'none'
					],
					'lname' => [
						'display' => 'Last Name',
						'required' => True,
						'special_chars' => 'none'
					],
					'username' => [
						'display' => 'Username',
						'required' => True,
						'unique' => 'users',
						'min' => 6,
						'max' => 150,
						'special_chars' => 'none'
					],
					'email' => [
						'display' => 'Email',
						'required' => True,
						'unique' => 'users',
						'max' => 150,
						'valid_email' => True
					],
					'password' => [
						'display' => 'Password',
						'required' => True,
						'min' => 6,
						'complexity' => True,
						'special_chars' => 'none'
					],
					'confirm' => [
						'display' => 'Confirm Password',
						'required' => True,
						'matches' => 'password'
					]
				]);

				if ($validation->passed()) {
					$newUser = new Users();
					SendEmail::confirmEmail($posted_values['email'], $posted_values['username'], $newUser); // send confirmation email before because "save()" in registerNewUser
					$newUser->registerNewUser($posted_values);
					$this->view->emailToBeConfirmed = $newUser->email;		
					$this->view->username =$newUser->username;
					//$newUser->login(); // Login, cannot access comment or like unless verified
					$this->view->render('register/verifyEmail');
					return ;
				}
			}

			$this->view->displayErrors = $validation->displayErrors();
			$this->view->post = $posted_values;
			$this->view->render('register/register');
		}

		public function confirmEmailAction($username = '', $token = '') {
			if ($username == '' || $token == '') {
				return False;
			} else {
				$usr = $this->UsersModel->findByUsername($username);
				if ($usr->username && $usr->token) {
					if ($usr->token == $token) {
						if ($usr->validateEmail()) {
							$this->view->validatedEmail = 1;
						} else {
							$this->view->validatedEmail = 0;
						}
					} else {
						$this->view->validatedEmail = 0;
					}
				} else {
					if ($usr->isEmailConfirmed) {
						$this->view->validatedEmail = 0;
					} else if ($usr->username) {
						$this->view->validatedEmail = 0;
					} else {
						Router::redirect('register/register');
						return;
					}
				}
			}
			$this->view->username = $username;
			$this->view->render('register/confirmedEmail');
		}

		public function resendConfirmEmailAction() {
			if ($_POST && isset($_POST['resendConfirmEmail'])) {
				if ($_POST['resendConfirmEmail'] != '') {
					$user = $this->UsersModel->findByUsername($_POST['resendConfirmEmail']);
					SendEmail::confirmEmail($user->email, $user->username, $user);
					$this->UsersModel->update($user->id, [
						'isEmailConfirmed' => 0,
						'token' => $user->token
					]);
					Router::redirect('register/login');
					return True;
				}
			}
			return False;
		}

		private function _forgotPasswordReset($token, $user) {
			$passToken = new ForgotPasswords($user->username);

			if ($passToken->token == $token) {
				$this->view->resetPassword = 1;

				if (isset($_POST['newPassword']) && isset($_POST['confirmNewPassword'])) {
					$validate = new Validate();
					$posted_values = posted_values($_POST);

					$validate->check($posted_values, [
						'newPassword' => [
							'display' => 'Password',
							'required' => True,
							'min' => 6,
							'complexity' => True,
							'special_chars' => 'none'
						],
						'confirmNewPassword' => [
							'display' => 'Confirm Password',
							'required' => True,
							'matches' => 'newPassword'
						]
					]);

					$this->view->displayErrors = $validate->displayErrors();
					if ($validate->passed()) {
						// update password in users table and redirect to login or log them in
						$password = $posted_values['newPassword'];
						$user->updatePassword($password);
						$this->ForgotPasswordsModel->deleteForgotPasswordEntry($user->username);
						$user->login();
						Router::redirect('');
					}
					unset($_POST['newPassword']);
					unset($_POST['confirmNewPassword']);
				}
			} else {
				$this->view->resetPassword = -1;
			}
		}

		public function forgotPasswordAction($username = '', $token = '') {
			$this->view->resetPassword = 0;
			$this->view->displayErrors = '';
			$this->view->username = $username;
			$this->view->resetPasswordToken = $token;
			$this->load_model('ForgotPasswords');

			if ($username != '' && $token != '') {
				$user = $this->UsersModel->findByUsername($username);
				if ($user->id) {
					$this->_forgotPasswordReset($token, $user);
				} else {
						$this->view->resetPassword = -1;
				}
			} else if (isset($_POST['resetPasswordUsername'])){
				$user = $this->UsersModel->findByUsername(Input::sanitize($_POST['resetPasswordUsername']));

				if ($user->id) {
					// Send email to reset password
					// TODO
					SendEmail::forgotPassword($user->email, $user->username, $user);
					// Add username and token to forgot_passwords table
					$this->ForgotPasswordsModel->insert([
						'username' => $user->username,
						'token' => $user->resetPasswordToken
					]);
					Router::redirect('register/login');
				} else {
					$this->view->resetPassword = -1;
				}
				unset($_POST['resetPasswordUsername']);
			}

			if ($this->view->resetPassword == -1) {
				$this->view->render('home/index');
			} else {
				$this->view->render('register/forgotPassword');
			}			
		}

		public function profileAction() {
			if (!currentUser()) {
				$this->view->render("register/register");
				return ;
			}

			$this->view->username = currentUser()->username;
			$this->view->email = currentUser()->email;
			$this->view->notifications = currentUser()->notifications == 1 ? "checked" : "";
			
			$validation = new Validate();
			$posted_values = [
				'username' => '',						
				'email' => '',
				'password' => '',
				'confirm' => ''
			];

			if ($_POST) {
				$posted_values = posted_values($_POST);

				$unchangedFields = [];

				if (currentUser()->username == $posted_values['username']) {
					$unchangedFields[] = "username";
				}

				if (currentUser()->email == $posted_values['email']) {
					$unchangedFields[] = "email";
				}

				$toCheck = [
					'username' => [
						'display' => 'Username',
						'required' => True,
						'unique' => 'users',
						'min' => 6,
						'max' => 150,
						'special_chars' => 'none'
					],
					'email' => [
						'display' => 'Email',
						'required' => True,
						'unique' => 'users',
						'max' => 150,
						'valid_email' => True
					],
					'password' => [
						'display' => 'Password',
						'required' => True,
						'min' => 6,
						'complexity' => True,
						'special_chars' => 'none'
					],
					'confirm' => [
						'display' => 'Confirm Password',
						'required' => 'True',
						'matches' => 'password',
						'complexity' => True,
						'special_chars' => 'none'
					]
				];

				foreach($unchangedFields as $val) {
					if (array_key_exists($val, $toCheck)) {
						unset($toCheck[$val]);
					}
				}

				$validation->check($posted_values, $toCheck);
				if ($validation->passed()) {
					// Update fields that changed
					foreach ($toCheck as $key => $val) {
						if ($key == 'email') {
							// Resend verification email if new email
							currentUser()->email = $posted_values['email'];
							SendEmail::confirmEmail($posted_values['email'], currentUser()->username, currentUser());
						} else if ($key == 'password') {
							currentUser()->password = password_hash($posted_values['password'], PASSWORD_DEFAULT);
						} else if ($key == 'username') {
							currentUser()->username = $posted_values['username'];
						}
					}
					if (isset($posted_values['notifications'])) {
						currentUser()->notifications = 1;
					} else {
						currentUser()->notifications = 0;
					}
					// Log user out
					currentUser()->save();
					currentUser()->logout();
					Router::redirect('');
				}
			}

			$this->view->displayErrors = $validation->displayErrors();
			$this->view->render("register/profile");
		}
	}