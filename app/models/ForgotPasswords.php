<?php
	class ForgotPasswords extends Model {
		public function __construct($username = '') {
			$table = 'forgot_password';
			parent::__construct($table);
			if ($username != '') {
				$u = $this->_db->findFirst($table, ['conditions' => 'username = ?', 'bind' => [$username]]);
				if ($u) {
					foreach ($u as $key => $val) {
						$this->$key = $val;
					}
				}
			}
		}

		public function deleteForgotPasswordEntry($username) {
			$sql = 'DELETE FROM `' . $this->_table . '` WHERE `username` = ?';
			$bind = [$username];
			$this->_db->query($sql, $bind);
		}
	}