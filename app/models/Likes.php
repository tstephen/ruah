<?php
	class Likes extends Model {
		public function __construct() {
			$table = 'likes';
			parent::__construct($table);
		}

		public function likeImage($image_id, $user_id) {
			$fields = [
				'user_id' => $user_id,
				'image_id' => $image_id
			];
			$this->insert($fields);
		}

		public function countLikes($image_id) {
			$sql = "SELECT * FROM " . $this->_table . " WHERE `image_id` = " . $image_id;
			return ($this->query($sql)->count());
		}

		public function alreadyLiked($imageId, $userId) {
			$params = [
				'conditions' => 'user_id = ? AND image_id = ?',
				'bind' => [$userId, $imageId]
			];

			return ($this->find($params));
		}
	}