<?php
	class Images extends Model {
		public function __construct() {
			$table = 'images';
			parent::__construct($table);
		}

		public function saveImage($name, $image, $type, $user) {
			$this->insert([
				"user_id" => $user->id,
				"name" => $name,
				"image" => $image,
				"type" => $type
			]);
		}



		public function getImages($user = '') {
			if (intval($user)){
				$conditions = "`user_id` = ?";
				$bind = [(int)$user];
				$fields = [
					'conditions' => $conditions,
					'bind' => $bind
				];
				return ($this->find($fields));
			} else {
				$sql = "SELECT * FROM `" . $this->_table . "`";
				return ($this->query($sql)->result());
			}
		}

		public function getImage($image_id) {
			$params = [
				'conditions' => '`id` = ?',
				'bind' => [$image_id]
			];
			return ($this->findFirst($params));
	
		}

		public function countImages() {
			return ($this->_db->count());
		}

		public function deleteImage($imageId) {
			$this->delete($imageId);
		}
	}