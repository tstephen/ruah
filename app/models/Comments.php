<?php
	class Comments extends Model {
		public function __construct() {
			$table = 'comments';
			parent::__construct($table);
		}

		public function addComment($userId, $imageId, $comment) {
			$fields = [
				"user_id" => $userId,
				"image_id" => $imageId,
				"comment" => $comment
			];
			$this->insert($fields);
		}

		public function getComments($imageId) {			
			$params = [
				"conditions" => "image_id = ?",
				"bind" => [$imageId]
			];

			return ($this->find($params));
		}

	}