<?php
	class SendEmail {

		private static $_tokenString = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890";

		private static function _generateToken() {
			$str = str_shuffle(self::$_tokenString);
			$str = substr($str, 0, 10);
			return $str;
		}

		public static function confirmEmail($email, $username, $user) {
			
			$token = self::_generateToken();
			$link = "http://" . getHostName() . ":8081". PROOT . 'Register/confirmEmail/';
			$link .= $username . '/';
			$link .= $token;

			$to = $email;
			$subject = 'Camagru | Please verify your Account';
			$headers = '';
			$headers .= "From: no-reply@camagru.co.za\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

			$message = '<html><body>';
			$message .= '<h1 style="text-center">Please verify your Account</h1><hr/><br/>';
			$message .= '<p>This email is intended for ' . $username . '</p>';
			$message .= '<p>Click on the link below to verify your Camagru Account</p>';
			$message .= '<a href="' . $link . '">' . $link . '</a>';
			$message .= '</body></html>';

			mail($to, $subject, $message, $headers);

			// Add token to the database
			$user->isEmailConfirmed = 0;
			$user->token = $token;
		}

		public static function forgotPassword($email, $username, $user) {
			$token = self::_generateToken();
			$link = "http://" . getHostName() . ":8081" . PROOT . 'Register/forgotPassword/';
			$link .= $username . '/';
			$link .= $token;

			$to = $email;
			$subject = 'Camagru | Reset Password';
			$headers = '';
			$headers .= "From: no-reply@camagru.co.za\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

			$message = '<html><body>';
			$message .= '<h1 style="text-center">Reset Your Password</h1><hr/><br/>';
			$message .= '<p>This email is intended for ' . $username . '</p>';
			$message .= '<p>Click on the link below to reset your Camagru password</p>';
			$message .= '<a href="' . $link . '">' . $link . '</a>';
			$message .= '</body></html>';

			mail($to, $subject, $message, $headers);

			// Add token to the database
			$user->resetPasswordToken = $token;
		}

		public static function sendNotification($email, $username, $user) {
			$to = $email;
			$subject = 'Camagru | Notification';
			$headers = '';
			$headers .= "From: no-reply@camagru.co.za\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

			$message = '<html><body>';
			$message .= '<h1 style="text-center">Notification</h1><hr/><br/>';
			$message .= '<p>This email is intended for ' . $username . '</p>';
			$message .= '<p>You have received a new comment on Camagru. Visit to view.</p>';
			$message .= '</body></html>';

			mail($to, $subject, $message, $headers);
		}
	}