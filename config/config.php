<?php
	define('DEBUG', True);
	
	define('DB_NAME', 'ruah'); // database name
	define('DB_USER', 'tstephen'); // database user
	define('DB_PASSWORD', '123456'); // database password
	define('DB_HOST', '127.0.0.1'); // database host
	
	define('DEFAULT_CONTROLLER', 'Home'); // default controller if there isn't one in the url
	define('DEFAULT_LAYOUT', 'default'); // if no layout is set in the controller use this layout
	
	define('PROOT', '/ruah/'); // set this to '/' for a live server

	define('SITE_TITLE', "Ruah MVC Framework"); // if no site title is set use this
	define('MENU_BRAND', 'RUAH');  // This is the brand text in the menu

	define('CURRENT_USER_SESSION_NAME', 'jUbDeNLAnfeAFVFoqfEF'); // session name for logged in user
	define('REMEMBER_ME_COOKIE_NAME', 'frfrEFWFONWFIefefwe46wefwEFEgpklpv'); // cookie name for logged in user
	define('REMEMBER_ME_COOKIE_EXPIRY', 2592000); // time in seconds for remember me cookie to expiry

	define('ACCESS_RESTRICTED', 'Restricted'); // Controller name for the restricted redirect