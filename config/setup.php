<?php
	require_once('database.php');

	$dbname = "ruah";

	try {
		$conn = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "CREATE DATABASE $dbname";
		$conn->exec($sql);
		echo "Database created successfully<br/>";
	} catch(PDOException $e) {
		echo $sql . "<br/>" . $e->getMessage() . "<br/>";
	}

	try {
		$conn = new PDO($DB_DSN . ";dbname=$dbname", $DB_USER, $DB_PASSWORD);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		echo "Connected successfully<br/>"; 
	} catch(PDOException $e) {
		echo "Connection failed: " . $e->getMessage() . "<br/>";
	}

	try {
		$conn = new PDO($DB_DSN . ";dbname=$dbname", $DB_USER, $DB_PASSWORD);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
		$sql = "CREATE TABLE contacts (
			`id` INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
			`lname` VARCHAR(150) DEFAULT NULL,
			`fname` VARCHAR(150) DEFAULT NULL,
			`email` VARCHAR(175) DEFAULT NULL,
			`phone1` VARCHAR(50) DEFAULT NULL,
			`phone2` VARCHAR(50) DEFAULT NULL,
			`phone3` VARCHAR(50) DEFAULT NULL,
			`address` VARCHAR(255) DEFAULT NULL,
			`address2` VARCHAR(255) DEFAULT NULL,
			`city` VARCHAR(255) DEFAULT NULL,
			`state` VARCHAR(150) DEFAULT NULL,
			`zip_code` VARCHAR(50) DEFAULT NULL,
			`country` VARCHAR(155) DEFAULT NULL,
			`user_id` INT(11) DEFAULT NULL
		)";
	
		$conn->exec($sql);
		echo "Table contacts created successfully<br/>";

		$sql = "CREATE TABLE `users` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`username` varchar(150) NOT NULL,
				`email` varchar(150) NOT NULL,
				`password` varchar(150) NOT NULL,
				`fname` varchar(150) NOT NULL,
				`lname` varchar(150) NOT NULL,
				`acl` text,
				`isEmailConfirmed` varchar(1) DEFAULT '0',
				`token` varchar(10) DEFAULT '',
				`deleted` varchar(1) DEFAULT '0',
				`notifications` varchar(1) DEFAULT '1',
				PRIMARY KEY (`id`)
				) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4";
	
		$conn->exec($sql);
		echo "Table users created successfully<br/>";

		$sql = "CREATE TABLE `user_sessions` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`uid` int(11) NOT NULL,
			`session` varchar(255) NOT NULL,
			`user_agent` varchar(255) NOT NULL,
			PRIMARY KEY (`id`)
		   )
		   ENGINE=InnoDB DEFAULT CHARSET=utf8";
	
		$conn->exec($sql);
		echo "Table user_sessions created successfully<br/>";
		
		$sql = "CREATE TABLE `ruah`.`forgot_password` (
			`id` INT NOT NULL AUTO_INCREMENT ,
			`username` VARCHAR(155) NULL ,
			`token` VARCHAR(10) NULL ,
			PRIMARY KEY (`id`)) ENGINE = InnoDB";
		
		$conn->exec($sql);
		echo "Table forgot_password created successfully<br/>";

		$sql = "CREATE TABLE `images` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`user_id` INT(11) NOT NULL,
			`name` varchar(50) NOT NULL,
			`type` varchar(50),
			`image` LONGBLOB NOT NULL,
			FOREIGN KEY (`user_id`) REFERENCES `users`(`id`) ON DELETE CASCADE,
			PRIMARY KEY (`id`)
		   ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4";

		$conn->exec($sql);
		echo "Table images created successfully<br/>";

		$sql = "CREATE TABLE `likes` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`user_id` int(11) NOT NULL,
			`image_id` int(11) NOT NULL,
			PRIMARY KEY (`id`),
			KEY `user_id` (`user_id`),
			KEY `image_id` (`image_id`),
			CONSTRAINT `likes_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
			CONSTRAINT `likes_ibfk_2` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`) ON DELETE CASCADE
		   ) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4";

		$conn->exec($sql);
		echo "Table likes created successfully<br/>";

		$sql = "CREATE TABLE `comments` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`user_id` int(11) NOT NULL,
			`image_id` int(11) NOT NULL,
			`comment` text NOT NULL,
			PRIMARY KEY (`id`),
			KEY `user_id` (`user_id`),
			KEY `image_id` (`image_id`),
			CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
			CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`) ON DELETE CASCADE
		   ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";

		$conn->exec($sql);
		echo "Table comments created successfully<br/>";


	} catch(PDOException $e) {
		echo $sql . "<br/>" . $e->getMessage() . "<br/>";
	}
?>